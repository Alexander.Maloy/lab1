package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public String givenList_shouldReturnARandomElement() {
        List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return randomElement;
    }


    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            
            System.out.println("Let's play round " + roundCounter);
            String PlayerInput = readInput("Your choice (Rock/Paper/Scissors)? ");
            PlayerInput = PlayerInput.toLowerCase();
            String computerChoise = givenList_shouldReturnARandomElement();
            String whoDidWin = whoWon(PlayerInput, computerChoise);

            System.out.println("Human chose " + PlayerInput + ", computer chose " + computerChoise + ". " + whoDidWin);
            

            if (!(rpsChoices.contains(PlayerInput))) {
                System.out.println("Invalid input! ");   
            }
            if (whoDidWin.equals("It's a tie!") == true) {
                roundCounter += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            if (whoDidWin.equals("Human wins!") == true) {
                humanScore += 1;
                roundCounter += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            if (whoDidWin.equals("Computer wins!") == true) {
                computerScore += 1;
                roundCounter += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            String wantToContinue = readInput("Do you wish to continue playing? (y/n)? ");
            if (wantToContinue.equals("n")) {
                System.out.println("Bye bye :) ");
                break;
            }
        }
         
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

public String whoWon(String PlayerInput, String computerChoise) {
    String whoWon = " ";

    if (!(rpsChoices.contains(PlayerInput))) {
        whoWon = " ";    
    }
    if (PlayerInput.equals(computerChoise) == true) {
        whoWon = "It's a tie!";   
    }
    if (PlayerInput.equals("rock") && computerChoise.equals("scissors")) { 
        whoWon = "Human wins!";   
    }
    if (PlayerInput.equals("rock") && computerChoise.equals("paper")) {
        whoWon = "Computer wins!";    
    }
    if (PlayerInput.equals("paper") && computerChoise.equals("rock")) {
        whoWon = "Human wins!";   
    }
    if (PlayerInput.equals("paper") && computerChoise.equals("scissors")) {
        whoWon = "Computer wins!";   
    }
    if (PlayerInput.equals("scissors") && computerChoise.equals("paper")) {
        whoWon = "Human wins!";  
    }
    if (PlayerInput.equals("scissors") && computerChoise.equals("rock")) {
        whoWon = "Computer wins!";
    }
    return (whoWon);
}

}



